package hust.soict.globalict.garbage;

import java.io.File;
import java.util.Scanner;
public class ReadString {

	public static void main(String[] args) {
		System.out.println(new File(".").getAbsolutePath());
		File file = new File("reallyLongText.txt");
	    Scanner sc = new Scanner(file);

	    while (sc.hasNextLine())
	      System.out.println(sc.nextLine());

	    sc.close();
	}

}
