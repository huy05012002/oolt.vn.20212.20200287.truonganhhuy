package hust.soict.globalict.lab02;
import java.util.Scanner;
public class SearchDayInMonth {
	Month[] months = new Month[12];
	
	public void main(String args[]) {
		months[0] = new Month("January", "Jan.", "Jan", 1, 31, 31);
		months[1] = new Month("February", "Feb.", "Feb", 2, 28, 29);
		months[2] = new Month("March", "Mar.", "Mar", 3, 31, 31);
		months[3] = new Month("April", "Apr.", "Apr", 4, 30, 30);
		months[4] = new Month("May", "May", "May", 5, 31, 31);
		months[5] = new Month("June", "June", "Jun", 6, 30, 30);
		months[6] = new Month("July", "July", "Jul", 7, 31, 31);
		months[7] = new Month("August", "Aug.", "Aug", 8, 31, 31);
		months[8] = new Month("September", "Sept.", "Sep", 9, 30, 30);
		months[9] = new Month("October", "Oct.", "Oct", 10, 31, 31);
		months[10] = new Month("November", "Nov.", "Nov", 11, 30, 30);
		months[11] = new Month("December", "Dec.", "Dec", 12, 31, 31);
		Scanner keyboard = new Scanner(System.in);
		
		Month foundMonth = null;
		
		while (foundMonth == null) {
			System.out.print("Enter month: ");
			String input = keyboard.nextLine();
			
			for (Month m: months) {
				if (m.monthName.equals(input)) {
					foundMonth = m;
					break;
				}
				if (m.Abb.equals(input)) {
					foundMonth = m;
					break;
				}
				if (m.shortCut.equals(input)) {
					foundMonth = m;
					break;
				}
				try {
					if (m.num == Integer.parseInt(input)) {
						foundMonth = m;
						break;
					}
				}
				catch (NumberFormatException ex){}
			}
			
			if (foundMonth == null)
				System.out.println("Invalid enter again\n");
		}
		
		int year;
		while(true) {
			System.out.print("Enter year: ");
			String yearString = keyboard.nextLine();
			try {
				year = Integer.parseInt(yearString);
				break;
			}
			catch(NumberFormatException e){
				System.out.println("Invalid year enter again\n");
			}
		}
		
		if (IsLeapYear(year))
			System.out.println(foundMonth.dayInLeap);
		else
			System.out.println(foundMonth.day);
	}
	
	
	public static Boolean IsLeapYear(int year) {
		if (year % 4 == 0)
			if (year % 100 == 0)
				if (year % 400 == 0)
					return true;
				else
					return false;
			else
				return true;
		else
			return false;
	}
}
