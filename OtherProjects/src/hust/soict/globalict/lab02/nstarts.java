package hust.soict.globalict.lab02;
import java.util.Scanner;
public class nstarts {
	public static void main(String[] args)
	{
		Scanner deezNut = new Scanner(System.in);
		
		System.out.println("n=");
		int n = deezNut.nextInt();
		
		for (int i = 0; i < n; i++) {
			for (int j = i; j < n; j++)
				System.out.print(' ');
			for (int j = 0; j < 2*i + 1; j++)
				System.out.print('*');
			for (int j = i; j < n; j++)
				System.out.print(' ');
			System.out.print('\n');
		}
	}
}
