package hust.soict.globalict.lab02;
public class Month {
	String monthName;
	String Abb;
	String shortCut;
	int num;
	int day;
	int dayInLeap;

public Month(String monthName, String Abb, String shortCut, int num, int day, int dayInLeap) {
	this.monthName = monthName;
	this.Abb = Abb;
	this.shortCut = shortCut;
	this.num = num;
	this.day = day;
	this.dayInLeap = dayInLeap;
}
}
