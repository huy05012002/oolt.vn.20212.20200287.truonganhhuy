package hust.soict.globalict.aims.utils.MyDate;
import java.time.format.DateTimeFormatter;
import java.time.LocalDate;
import java.util.Locale;
import java.util.Scanner;
import java.time.LocalDateTime;

public class MyDate {
	private int day;
	private int month;
	private int year;
	
	public int getDay() {
		return this.day;
	}
	
	public void setDay(int day) {
		this.day = day;
	}
	
	public int getMonth() {
		return this.month;
	}
	
	public void setMonth(int month) {
		this.month = month;
	}
	
	public int getYear() {
		return this.year;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public MyDate GetDate() {
		return this;
	}
	
	public MyDate() {
		this.day = LocalDateTime.now().getDayOfMonth();
		this.month = LocalDateTime.now().getMonthValue();
		this.year = LocalDateTime.now().getYear();
	}
	
	
	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public MyDate(String date) {
		setDate(date);
	}
	
	public void setDate(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM d['st']['nd']['rd']['th'] y", Locale.ENGLISH);
		LocalDate dateTime = LocalDate.parse(date, formatter);
		this.setDate(dateTime);
	}
	
	public void setDate(LocalDate dateTime) {
		this.day = dateTime.getDayOfMonth();
		this.month = dateTime.getMonthValue();
		this.year = dateTime.getYear();
	}
	
	public void accept() {
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			System.out.print("Date: ");
			String input = sc.nextLine();
			try {
				setDate(input);
				break;
			} catch(Exception e) {
				System.out.println("Wrong date format");
				continue;
			}
		}
		
		//sc.close();
	}
	
	public void print() {
		LocalDate date = LocalDate.of(year, month, day);
		System.out.println(date);
	}
	
	public void printOfPattern() {
		LocalDate date = LocalDate.of(year, month, day);
		
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			System.out.print("Format: ");
			System.out.println();
			String pattern = sc.nextLine();
			try {
				System.out.println(date.format(DateTimeFormatter.ofPattern(pattern)));
				break;
			} catch(Exception e) {
				System.out.println("Wrong date format");
				continue;
			}
		}
		
		sc.close();
	}
	
	public static void main(String[] args) {
		MyDate today = new MyDate();
		today.print();
		
		MyDate intDate = new MyDate(5, 1, 2002);
		intDate.print();
		
		MyDate stringDate = new MyDate("May 6th 1976");
		stringDate.print();
		
		MyDate input = new MyDate();
		input.accept();
		input.printOfPattern();
	}
}
