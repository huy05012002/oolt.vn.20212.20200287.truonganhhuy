package hust.soict.globalict.aims.order.Order;
import javax.swing.JOptionPane;

import hust.soict.globalict.aims.disc.DigitalVideoDisc.DigitalVideoDisc;
import hust.soict.globalict.aims.utils.MyDate.MyDate;
public class Order {
	public static final int MAX_NUMBER_ORDERED = 10;
	public static final int MAX_LIMITED_ORDERS = 3;
	private static int nbOrders = 0;
	public int  qtyOrdered;
	private MyDate dateOrdered;
	private DigitalVideoDisc itemOrdered[];
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOrdered == MAX_NUMBER_ORDERED)
			JOptionPane.showMessageDialog(null, "Order fulled");
		else {
			itemOrdered[qtyOrdered] = disc;
			qtyOrdered++;
		}		
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList) {
		if(qtyOrdered + dvdList.length > MAX_NUMBER_ORDERED)
			JOptionPane.showMessageDialog(null, "Not enough space");
		else {
			for (DigitalVideoDisc disc : dvdList) {
				itemOrdered[qtyOrdered] = disc;
				qtyOrdered++;
			}
		}		
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		if(qtyOrdered == MAX_NUMBER_ORDERED) {
			JOptionPane.showMessageDialog(null, "Order fulled! Couldn't add " + dvd1.getTitle() + " and " + dvd2.getTitle());
		}
		else {
			itemOrdered[qtyOrdered] = dvd1;
			qtyOrdered++;
			if(qtyOrdered == MAX_NUMBER_ORDERED) {
				JOptionPane.showMessageDialog(null, "Order fulled! Couldn't add " + dvd2.getTitle());
			}
			else {
				itemOrdered[qtyOrdered] = dvd2;
				qtyOrdered++;
			}
		}		
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		JOptionPane.showMessageDialog(null, "Function not emplemented yet");
	}
	
	public float totalCost() {
		float total = 0;
		for(int i = 0; i < qtyOrdered; i++) {
			total = total + itemOrdered[i].getCost();
		}
		return total;
	}
	
	public float totalCost(DigitalVideoDisc freeDisc) {
		float total = 0;
		for(int i = 0; i < qtyOrdered; i++) {
			if(itemOrdered[i] != freeDisc)
			total = total + itemOrdered[i].getCost();
		}
		return total;
	}
	
	public Order() {
		nbOrders ++;
		if (nbOrders <= MAX_LIMITED_ORDERS) {
			itemOrdered = new DigitalVideoDisc[MAX_NUMBER_ORDERED];
			dateOrdered = new MyDate();
			qtyOrdered = 0;
		}else {
			System.out.println("Order limit reached");
		}
	}
	
	public void print() {
		System.out.println("***********************Order***********************");
		System.out.println("Date: " + dateOrdered.getDay() + "-" + dateOrdered.getMonth() + "-" + dateOrdered.getYear());
		System.out.println("Ordered Items:");
		for (int i = 0; i < qtyOrdered; i++){
			System.out.println("DVD - " + itemOrdered[i].getTitle() + " - " + itemOrdered[i].getCategory() + " - " + itemOrdered[i].getDirectory() + " - " + itemOrdered[i].getLength() + ": " + itemOrdered[i].getCost() + "$");
		}
		System.out.println("Total cost: " + totalCost());
		System.out.println("***************************************************");
	}
	
	public void print(DigitalVideoDisc freeDisc) {
		System.out.println("***********************Order***********************");
		System.out.println("Date: " + dateOrdered.getDay() + "-" + dateOrdered.getMonth() + "-" + dateOrdered.getYear());
		System.out.println("Ordered Items:");
		for (int i = 0; i < qtyOrdered; i++){
			if (freeDisc == itemOrdered[i])
				System.out.println("DVD - " + itemOrdered[i].getTitle() + " - " + itemOrdered[i].getCategory() + " - " + itemOrdered[i].getDirectory() + " - " + itemOrdered[i].getLength() + ": " + itemOrdered[i].getCost() + "$ - this is a free item");
			else 
				System.out.println("DVD - " + itemOrdered[i].getTitle() + " - " + itemOrdered[i].getCategory() + " - " + itemOrdered[i].getDirectory() + " - " + itemOrdered[i].getLength() + ": " + itemOrdered[i].getCost() + "$");
		}
		System.out.println("Total cost: " + totalCost(freeDisc));
		System.out.println("***************************************************");
	}
	
	public int getnbOrders() {
		return nbOrders;
	}
	
	public DigitalVideoDisc getALuckyItem() {
		int freeDiscIndex = (int) Math.random() * qtyOrdered;
		DigitalVideoDisc freeDisc = itemOrdered[freeDiscIndex];
		return freeDisc;
	}
}
