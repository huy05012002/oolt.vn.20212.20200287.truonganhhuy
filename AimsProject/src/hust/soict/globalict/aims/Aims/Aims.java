package hust.soict.globalict.aims.Aims;
import javax.swing.JOptionPane;

import hust.soict.globalict.aims.disc.DigitalVideoDisc.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order.Order;

public class Aims {

	public static void main(String[] args) {
		Order anOrder = new Order();
		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirectory("Roger Allers");
		dvd1.setLength(87);
		anOrder.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirectory("George Lucas");
		dvd2.setLength(124);
		anOrder.addDigitalVideoDisc(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirectory("John Musker");
		dvd3.setLength(90);
		anOrder.addDigitalVideoDisc(dvd3);
		
		//anOrder.addDigitalVideoDisc(dvd3);

		JOptionPane.showMessageDialog(null, "Total cost is: " + anOrder.totalCost() + anOrder.qtyOrdered);
		anOrder.print();
		
		Order secondOrder = new Order();
		Order thirdOrder = new Order();
		
		Order forthOrder = new Order();
		try {
			forthOrder.addDigitalVideoDisc(dvd1);
		}catch(Exception e) {
			System.out.println("Cant add");
		}
		
		
		System.out.println(forthOrder.getnbOrders());
	}

}
