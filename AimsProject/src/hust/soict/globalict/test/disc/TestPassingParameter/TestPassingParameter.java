package hust.soict.globalict.test.disc.TestPassingParameter;
import hust.soict.globalict.aims.disc.DigitalVideoDisc.DigitalVideoDisc;

public class TestPassingParameter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
		DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");
		
		swap(jungleDVD, cinderellaDVD);
		trueSwap(jungleDVD, cinderellaDVD);
		System.out.println("jungle dvd title: " + jungleDVD.getTitle());
		System.out.println("cinderella dvd title: " + cinderellaDVD.getTitle());
		
		changeTitle(jungleDVD, cinderellaDVD.getTitle());
		System.out.println("jungle dvd title: " + jungleDVD.getTitle());
	}
	
	public static void swap(Object o1, Object o2) {
		Object tmp = o1;
		o1 = o2;
		o2 = tmp;
	}
	
	public static void changeTitle(DigitalVideoDisc dvd, String title) {
		String oldTitle = dvd.getTitle();
		dvd.setTitle(title);
		dvd = new DigitalVideoDisc(oldTitle);
	}
	
	public static void trueSwap(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		DigitalVideoDisc temp = new DigitalVideoDisc(dvd1.getTitle());
		temp.setCategory(dvd1.getCategory());
		temp.setCost(dvd1.getCost());
		temp.setDirectory(dvd1.getDirectory());
		temp.setLength(dvd1.getLength());
		
		dvd1.setTitle(dvd2.getTitle());
		dvd1.setCategory(dvd2.getCategory());
		dvd1.setCost(dvd2.getCost());
		dvd1.setDirectory(dvd2.getDirectory());
		dvd1.setLength(dvd2.getLength());
		
		dvd2.setTitle(temp.getTitle());
		dvd2.setCategory(temp.getCategory());
		dvd2.setCost(temp.getCost());
		dvd2.setDirectory(temp.getDirectory());
		dvd2.setLength(temp.getLength());
		
	}

}
